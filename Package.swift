// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SevAllPersistenceLayer",
    platforms: [
        .iOS(.v14),
        .macOS(.v12)
    ],
    products: [
        .library(
            name: "SevAllPersistenceLayer",
            targets: ["SevAllPersistenceLayer"]),
    ],
    dependencies: [
        .package(url: "https://gitlab.com/milos.dimic/SevAllNetworkLayer.git", .upToNextMajor(from: "0.0.1"))
    ],
    targets: [
        .target(
            name: "SevAllPersistenceLayer",
            dependencies: [.product(name: "SevAllNetworkLayer", package: "SevAllNetworkLayer")]),
        .testTarget(
            name: "SevAllPersistenceLayerTests",
            dependencies: ["SevAllPersistenceLayer"]),
    ]
)
